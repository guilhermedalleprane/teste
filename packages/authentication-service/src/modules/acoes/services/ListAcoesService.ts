import {Acao} from '../domain/model/Acao';
import {IAcoesRepository} from '../domain/repositories/IAcoesRepository';

type SortType = 'asc' | 'desc'

interface IRequest {
  page: number;
  pageSize: number;
  search?: string;
  sorting?: {
    [key: string]: SortType;
  };
}

interface IResponse {
  rows: Acao[];
  page: number;
  total: number;
  pageSize: number;
  totalPages: number;
}

/**
    * Serviço de listagem de ações.
    */
export class ListAcoesService {
  private acoesRepository: IAcoesRepository;

  /**
   * Contrutor de ações.
   * @param {object} acoesRepository - Repositório das ações
   */
  constructor(acoesRepository: IAcoesRepository) {
    this.acoesRepository = acoesRepository;
  }

  /**
    * Efetua a listagem de ações.
    * @param {object} data - Dados da tabela de ações.
    * @return {object} - A listagem das ações.
    */
  async execute(data: IRequest = {page: 1, pageSize: 15}): Promise<IResponse> {
    const searchFields = ['nivel'];

    const rows = await this.acoesRepository.list({
      paging: {
        page: data.page,
        size: data.pageSize,
      },
      search: data?.search && {
        fields: searchFields,
        type: 'contains',
        value: data.search,
      },
      sorting: data?.sorting,
    });

    const total = await this.acoesRepository.count({
      search: data?.search && {
        fields: searchFields,
        type: 'contains',
        value: data.search,
      },
    });

    return {
      rows,
      total,
      page: data.page,
      pageSize: data.pageSize,
      totalPages: Math.ceil(total / data.pageSize),
    };
  }
}
