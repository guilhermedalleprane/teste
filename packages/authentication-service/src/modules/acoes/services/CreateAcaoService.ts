import {IAcoesRepository} from '../domain/repositories/IAcoesRepository';

interface IRequest {
  chave: string;
  nivel: string;
}
interface IResponse {
  id: string;
  chave: string;
  nivel: string;
}

/**
  * Serviço de criação de ações.
  */
export class CreateAcaoService {
  private acoesRepository: IAcoesRepository;

  /**
   * Construtor de ações.
   * @param {object} acoesRepository - Repositório de ações
   */
  constructor(acoesRepository: IAcoesRepository) {
    this.acoesRepository = acoesRepository;
  }

  /**
    * Efetua a criação de um ação.
    * @param {object} data - Dados da ação.
    * @return {object} - A ação criada.
    */
  async execute(data: IRequest): Promise<IResponse> {
    const acao = await this.acoesRepository.create(data);

    return {
      id: acao.id,
      chave: acao.chave,
      nivel: acao.nivel,
    };
  }
}
