import {Acao} from '../../../domain/model/Acao';
import {IAcoesRepository} from '../../../domain/repositories/IAcoesRepository';
import {CountAcoesDTO} from '../../../dtos/CountAcoesDTO';
import {ListAcoesDTO} from '../../../dtos/ListAcoesDTO';

/**
 * Repositório falso de usuários utilizado para testes unitários
 */
export class FakeAcoesRepository implements IAcoesRepository {
  private acoes: Acao[] = [];

  /**
   * Cria uma ação
   * @param {object} data - Dados para a criação de uma ação
   * @return {object} - Retorna um objeto criado
   */
  async create(data: Omit<Acao, 'id'>): Promise<Acao> {
    const acao = new Acao({
      id: Date.now().toString(),
      ...data,
    });
    this.acoes.push(acao);
    return acao;
  }

  /**
   * Atualiza uma ação
   * @param {string} id - ID da ação a ser atualizada
   * @param {object} data - Dados para a atualização da ação
   * @return {object} - Retorna a ação atualizada
   */
  async update(id: string, data: Partial<Acao>) {
    let u: Acao;

    const notExists = this.acoes.every((acao, i) => {
      if (acao.id == id) {
        Object.assign(acao[i], data);
        u = acao[i];
        return false;
      }
      return true;
    });

    if (notExists) {
      throw new Error('Usuário não encontrado');
    }

    return u;
  }

  /**
   * Busca uma ação
   * @return {array} - Retorna um array com as ações encontradas
   */
  async find() {
    return this.acoes;
  }

  /**
   * Lista asa ções
   * @param {object} options - Parâmetro adicional para a listagem de ações
   * @return {array} - Retorna a list de ações
   */
  async list(options?: ListAcoesDTO): Promise<Acao[]> {
    let data = this.acoes.slice();


    if (options?.search) {
      let regex: RegExp;

      switch (options.search.type) {
        case 'startsWith':
          regex = new RegExp(`^${options.search.value}`);
          break;
        case 'endWith':
          regex = new RegExp(`${options.search.value}$`);
          break;
        case 'contains':
          regex = new RegExp(`^${options.search.value}$`);
          break;
      }

      if (options?.search?.fields && regex) {
        data = data.filter((acao) =>
          !options.search.fields.every((field) =>
            !regex.test(acao[field]),
          ),
        );
      }
    }

    if (options?.sorting) {
      data = data.sort((a, b) =>
        Object.keys(options.sorting).reduce((bool, k) => {
          return bool || (
            (options.sorting[k] == 'asc' && (a[k] > b[k] ? 1 : -1)) ||
            (options.sorting[k] == 'desc' && (a[k] > b[k] ? -1 : 1))
          );
        }, 0),
      );
    }

    if (options?.paging) {
      data = data.slice((options.paging.page - 1) * options.paging.size, options.paging.page * options.paging.size);
    }

    return data;
  }

  /**
   * Conta a quantidade de ações
   * @param {object} options - Parâmetro adicional para a contagem de ações
   * @return {number} - Retorna o número de ações
   */
  async count(options?: CountAcoesDTO): Promise<number> {
    let data = this.acoes.slice();

    if (options?.search) {
      let regex: RegExp;

      switch (options.search.type) {
        case 'startsWith':
          regex = new RegExp(`^${options.search.value}`);
          break;
        case 'endWith':
          regex = new RegExp(`${options.search.value}$`);
          break;
        case 'contains':
          regex = new RegExp(`^${options.search.value}$`);
          break;
      }

      if (options.search.fields && regex) {
        data = data.filter((acao) =>
          !options.search.fields.every((field) =>
            !regex.test(acao[field]),
          ),
        );
      }
    }

    return data.length;
  }
}
