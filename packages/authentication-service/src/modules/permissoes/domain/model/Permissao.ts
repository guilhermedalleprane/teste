/**
 * Representa uma permissão da aplicação.
 */
export class Permissao {
  id: string;
  chave: string;
  descricao?: string;
  acoes?: string[];

  /**
 * Cria uma permissão.
 * @param {Partial<Permissao>} permissao - Atributos da permissão.
 */
  constructor(permissao: Partial<Permissao>) {
    this.id = permissao.id;
    this.chave = permissao.chave;
    this.descricao = permissao.descricao;
    this.acoes = permissao.acoes;
  }
}
