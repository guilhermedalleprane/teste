import {CountPermissoesDTO} from '../../dtos/CountPermissoesDTO';
import {ListPermissoesDTO} from '../../dtos/ListPermissoesDTO';
import {Permissao} from '../model/Permissao';

export interface IPermissoesRepository {

  /**
    * Cria uma permissão no sistema.
    * @param data - Dados da permissão que será criada.
    * @returns Permissão criada.
    */
  create(data: Omit<Permissao, 'id'>): Promise<Permissao>;

  /**
    * Lista as permissões do sistema.
    * @param options - Parâmetro opcional para pesquisa, paginação e ordenação.
    * @returns Array contendo as permissões.
    */
  list(options?: ListPermissoesDTO): Promise<Permissao[]>;

  /**
    * Conta as permissões do sistema.
    * @param options - Parâmetro opcional para pesquisa.
    * @returns A quantidade de permissões.
    */
  count(options?: CountPermissoesDTO): Promise<number>;
}
