import {Permissao} from '../domain/model/Permissao';
import {IPermissoesRepository} from '../domain/repositories/IPermissoesRepository';

type SortType = 'asc' | 'desc'

interface IRequest {
  page: number;
  pageSize: number;
  search?: string;
  sorting?: {
    [key: string]: SortType;
  };
}

interface IResponse {
  rows: Permissao[];
  page: number;
  total: number;
  pageSize: number;
  totalPages: number;
}

/**
    * Serviço de listagem de permissões.
    */
export class ListPermissoesService {
  private permissoesRepository: IPermissoesRepository;

  /**
   * Construtor para a criação de permissões
   * @param {object} permissoesRepository - Repositório de permissões
   */
  constructor(permissoesRepository: IPermissoesRepository) {
    this.permissoesRepository = permissoesRepository;
  }

  /**
    * Efetua a listagem de permissões.
    * @param {object} data - Dados da tabela de permissões.
    * @return {object} - Retorna os resultados encontrados.
    */
  async execute(data: IRequest = {page: 1, pageSize: 15}): Promise<IResponse> {
    const searchFields = ['chave'];

    const rows = await this.permissoesRepository.list({
      paging: {
        page: data.page,
        size: data.pageSize,
      },
      search: data?.search && {
        fields: searchFields,
        type: 'contains',
        value: data.search,
      },
      sorting: data?.sorting,
    });

    const total = await this.permissoesRepository.count({
      search: data?.search && {
        fields: searchFields,
        type: 'contains',
        value: data.search,
      },
    });

    return {
      rows,
      total,
      page: data.page,
      pageSize: data.pageSize,
      totalPages: Math.ceil(total / data.pageSize),
    };
  }
}
