import {IPesquisa} from '@shared/domain/IPesquisa';

export interface CountPermissoesDTO {
  search: IPesquisa;
}
