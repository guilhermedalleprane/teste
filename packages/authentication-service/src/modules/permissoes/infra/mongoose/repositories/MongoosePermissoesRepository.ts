import {Permissao} from '../../../domain/model/Permissao';
import {IPermissoesRepository} from '../../../domain/repositories/IPermissoesRepository';
import {CountPermissoesDTO} from '../../../dtos/CountPermissoesDTO';
import {ListPermissoesDTO} from '../../../dtos/ListPermissoesDTO';
import {PermissaoModel} from '../entities/PermissaoModel';
import {BaseRepository} from './BaseRepository';

/**
 * Repositório das permissões utilizando o Mongoose
 */
export class MongoosePermissoesRepository implements IPermissoesRepository {
  private repository: BaseRepository

  /**
   *
   */
  constructor() {
    this.repository = new BaseRepository(PermissaoModel);
  }

  /**
   * Cria uma permissão
   * @param {object} data - Dados da permissão que será criada
   * @return {object} - Retorna a permissão criada
   */
  async create(data: Omit<Permissao, 'id'>): Promise<Permissao> {
    const doc = await this.repository.create(data);
    return new Permissao(doc);
  }

  /**
   * Realiza a listagem de permissões
   * @param {object} options - Parâmetro adicional para a listagem de objetos
   * @return {array} - Retorna um array com os objetos selecionados
   */
  async list(options?: ListPermissoesDTO): Promise<Permissao[]> {
    const docs = await this.repository.find({}, options);
    const data: Permissao[] = [];

    docs.forEach((doc) => {
      data.push(new Permissao(doc));
    });

    return data;
  }

  /**
   * Realiza a contagem de permissões
   * @param {object} options - Parâmetro adicional para a contagem de permissões
   * @return {number} - Retorna a quantidade de permissões
   */
  async count(options?: CountPermissoesDTO): Promise<number> {
    return this.repository.count(options);
  }
}
