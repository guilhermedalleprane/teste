import mongoose from 'mongoose';
import {Papel} from '../../../domain/model/Papel';

/**
 * Modelo do papel
 */
const opts: mongoose.SchemaOptions = {
  collection: 'papeis',
  toJSON: {
    virtuals: true,
  },
  timestamps: {},
};

const schema = new mongoose.Schema({
  chave: {
    type: String,
    required: true,
  },

  descricao: {
    type: String,
    required: true,
  },

  permissoes: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Permissoes',
  }],
}, opts);

export const PapelModel = mongoose.model<Papel & mongoose.Document>('Papel', schema);
