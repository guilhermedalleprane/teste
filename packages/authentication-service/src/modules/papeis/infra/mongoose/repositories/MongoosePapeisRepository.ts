import {Papel} from '../../../domain/model/Papel';
import {IPapeisRepository} from '../../../domain/repositories/IPapeisRepository';
import {CountPapeisDTO} from '../../../dtos/CountPapeisDTO';
import {ListPapeisDTO} from '../../../dtos/ListPapeisDTO';
import {PapelModel} from '../entities/PapelModel';
import {BaseRepository} from './BaseRepository';

/**
 * Repositório dos papeis utilizando Mongoose
 */
export class MongoosePapeisRepository implements IPapeisRepository {
  private repository: BaseRepository

  /**
   * Construtor de papeis
   */
  constructor() {
    this.repository = new BaseRepository(PapelModel);
  }

  /**
   * Cria um papel
   * @param {object} data - Dados para a criação do papel
   * @return {object} - Retorna o papel criado
   */
  async create(data: Omit<Papel, 'id'>): Promise<Papel> {
    const doc = await this.repository.create(data);
    return new Papel(doc);
  }

  /**
   * Listagem dos papeis
   * @param {object} options - Opções a serem utilizadas na listagem dos papeis.
   * @return {array} - Array com os papeis
   */
  async list(options?: ListPapeisDTO): Promise<Papel[]> {
    const docs = await this.repository.find({}, options);
    const data: Papel[] = [];

    docs.forEach((doc) => {
      data.push(new Papel(doc));
    });

    return data;
  }

  /**
   * Realiza a contagem de papeis
   * @param {object} options - Opções a serem utilizadas na contagem dos papeis
   * @return {number} - Retorna a quantidade de papeis.
   */
  async count(options?: CountPapeisDTO): Promise<number> {
    return this.repository.count(options);
  }
}
