import {IOrdenacao} from '@shared/domain/IOrdenacao';
import {IPaginacao} from '@shared/domain/IPaginacao';
import {IPesquisa} from '@shared/domain/IPesquisa';
import {Document, FilterQuery, Model} from 'mongoose';

type FindOptions = {
  paging?: IPaginacao;
  search?: IPesquisa;
  sort?: IOrdenacao;
}

type CountOptions = {
  search?: IPesquisa;
}

/**
  * Repositório genérico para manipulação de dados
  */
export class BaseRepository {
  private model: Model<Document>

  /**
   * Construtor de papeis
   * @param {object} model - Objeto que utilizará o modelo do papel.
   */
  constructor(model) {
    this.model = model;
  }

  /**
    * Realiza a inclusão de um registro
    * @param {object} data - Dados do registro a ser incluído
    * @return {object} O registro incluído
    */
  async create(data: object): Promise<unknown> {
    return this.model.create(data);
  }

  /**
    * Realiza a atualização de um registro
    * @param {string} id - ID do registro a ser atualizado
    * @param {object} data - Dados a serem atualizados
    * @return {object} O registro atualizado
    */
  async update(id: string, data: object): Promise<unknown> {
    return this.model.findByIdAndUpdate(id, {$set: {...data}});
  }

  /**
    * Realiza a busca de registros
    * @param {object} filter - Filtros a serem utilizados na busca
    * @param {object} options - Opções a serem utilizadas na busca
    * @return {array} Array contendo registros encontrados
    */
  async find(filter: FilterQuery<unknown>, options?: FindOptions): Promise<unknown[]> {
    const query = this.model.find(filter);

    if (options?.search) {
      let regex: RegExp;

      switch (options?.search?.type) {
        case 'startsWith':
          regex = new RegExp(`^${options.search.value}`);
          break;
        case 'endWith':
          regex = new RegExp(`${options.search.value}$`);
          break;
        case 'contains':
          regex = new RegExp(`^${options.search.value}$`);
          break;
      }

      if (options?.search?.fields && regex) {
        query.find({
          $or: options.search.fields.map((f) => (
            {
              [f]: regex,
            }
          )),
        });
      }
    }

    if (options?.paging) {
      query.skip((options.paging.page - 1) * options.paging.size);
      query.limit(options.paging.size);
    }

    if (options?.sort) {
      query.sort(options.sort);
    }

    return query.exec();
  }

  /**
    * Realiza a contagem de registros.
    * @param {object} options - opções a serem utilizadas na contagem.
    * @return {number} Quantidade de registros.
    */
  async count(options?: CountOptions): Promise<number> {
    const query = this.model.find();

    if (options?.search) {
      let regex;

      switch (options.search.type) {
        case 'startsWith':
          regex = new RegExp(`^${options.search.value}`);
          break;
        case 'endWith':
          regex = new RegExp(`${options.search.value}$`);
          break;
        case 'contains':
          regex = new RegExp(`^${options.search.value}$`);
          break;
      }

      if (options.search.fields && regex) {
        query.find({
          $or: options.search.fields.map((f) => (
            {
              [f]: regex,
            }
          )),
        });
      }
    }

    return query.countDocuments().exec();
  }
}
