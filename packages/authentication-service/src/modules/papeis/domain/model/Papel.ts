/**
 * Representa um papel na aplicação.
 */
export class Papel {
  id: string;
  chave: string;
  descricao?: string;
  permissoes?: string[];

  /**
   * Cria um papel.
   * @param {Partial<Papel>}papel - Atributos do papel.
   */
  constructor(papel: Partial<Papel>) {
    this.id = papel.id;
    this.chave = papel.chave;
    this.descricao = papel.descricao;
    this.permissoes = papel.permissoes;
  }
}
