import {FakePapeisRepository} from '../domain/repositories/fakes/FakePapeisRepository';
import {CreatePapelService} from './CreatePapelService';

describe('CreatePapelService', () => {
  it('should be able to create an action', async () => {
    const repository = new FakePapeisRepository();
    const service = new CreatePapelService(repository);

    const papel = await service.execute({
      chave: 'John Doe',
      descricao: '12312312387',
      permissoes: ['johndoe@foobar.com'],
    });

    expect(papel).toBeDefined();
    expect(papel).toHaveProperty('id');
    expect(papel.chave).toBe('John Doe');
    expect(papel.descricao).toBe('12312312387')
    expect(papel.permissoes).toContain('johndoe@foobar.com')
  })
})
