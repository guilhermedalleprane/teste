import {Papel} from '../domain/model/Papel';
import {IPapeisRepository} from '../domain/repositories/IPapeisRepository';

type SortType = 'asc' | 'desc'

interface IRequest {
  page: number;
  pageSize: number;
  search?: string;
  sorting?: {
    [key: string]: SortType;
  };
}

interface IResponse {
  rows: Papel[];
  page: number;
  total: number;
  pageSize: number;
  totalPages: number;
}

/**
    * Serviço de listagem de papeis.
    */
export class ListPapeisService {
  private papeisRepository: IPapeisRepository;

  /**
   * Construtor dos papeis utilizando repositório e interface
   * @param {object} papeisRepository
   */
  constructor(papeisRepository: IPapeisRepository) {
    this.papeisRepository = papeisRepository;
  }

  /**
    * Efetua a listagem dos papeis.
    * @param {object} data - Dados da tabela de papeis.
    * @return {object} - Retorna a lista de papeis.
    */
  async execute(data: IRequest = {page: 1, pageSize: 15}): Promise<IResponse> {
    const searchFields = ['chave'];

    const rows = await this.papeisRepository.list({
      paging: {
        page: data.page,
        size: data.pageSize,
      },
      search: data?.search && {
        fields: searchFields,
        type: 'contains',
        value: data.search,
      },
      sorting: data?.sorting,
    });

    const total = await this.papeisRepository.count({
      search: data?.search && {
        fields: searchFields,
        type: 'contains',
        value: data.search,
      },
    });

    return {
      rows,
      total,
      page: data.page,
      pageSize: data.pageSize,
      totalPages: Math.ceil(total / data.pageSize),
    };
  }
}
