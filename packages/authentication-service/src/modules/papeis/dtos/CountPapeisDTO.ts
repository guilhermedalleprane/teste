import {IPesquisa} from '@shared/domain/IPesquisa';

export interface CountPapeisDTO {
  search: IPesquisa;
}
