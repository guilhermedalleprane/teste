import {IPesquisa} from '@shared/domain/IPesquisa';

export interface CountUsuariosDTO {
  search: IPesquisa;
}
