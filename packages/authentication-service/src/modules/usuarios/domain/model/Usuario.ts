/**
 * Representa um usuário da aplicação.
 */
export class Usuario {
  id: string;
  nome: string;
  cpf?: string;
  email?: string;
  telefone?: string;
  papeis?: string[];

  /**
    * Cria um usuário
    * @param {Partial<Usuario>} usuario - Atributos do usuário.
    */
  constructor(usuario: Partial<Usuario>) {
    this.id = usuario.id;
    this.nome = usuario.nome;
    this.cpf = usuario.cpf;
    this.email = usuario.email;
    this.telefone = usuario.telefone;
    this.papeis = usuario.papeis;
  }
}
