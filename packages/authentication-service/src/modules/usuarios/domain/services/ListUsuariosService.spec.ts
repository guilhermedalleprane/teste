import faker from 'faker';
import {FakeUsuariosRepository} from '../repositories/fakes/FakeUsuariosRepository';
import {ListUsuariosService} from './ListUsuariosService';

describe('ListUsuarioService', () => {
  const repository = new FakeUsuariosRepository();

  beforeEach(async () => {
    for (let i = 0; i < 20; i++) {
      await repository.create({
        nome: faker.name.findName(),
        email: faker.internet.email(),
      });
    }
  });

  it('should be able to list users', async () => {
    const service = new ListUsuariosService(repository);

    const result = await service.execute({
      page: 1,
      pageSize: 10,
    });

    expect(result).toBeDefined();
    expect(result.rows).toHaveLength(10);
    expect(result.total).toBe(20);
    expect(result.page).toBe(1);
    expect(result.totalPages).toBe(2);
  });
});
