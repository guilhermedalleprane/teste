import {CountUsuariosDTO} from '../../dtos/CountUsuariosDTO';
import {ListUsuariosDTO} from '../../dtos/ListUsuariosDTO';
import {Usuario} from '../model/Usuario';

export interface IUsuariosRepository {

  /**
    * Cria um usuário no sistema.
    * @param data - Dados do usuário que será criado.
    * @returns Usuário criado.
    */
  create(data: Omit<Usuario, 'id'>): Promise<Usuario>;

  /**
    * Lista os usuários do sistema.
    * @param options - Parâmetro opcional para pesquisa, paginação e ordenação.
    * @returns Array contendo os usuários.
    */
  list(options?: ListUsuariosDTO): Promise<Usuario[]>;

  /**
    * Conta os usuários do sistema.
    * @param options - Parâmetro opcional para pesquisa.
    * @returns A quantidade de usuários.
    */
  count(options?: CountUsuariosDTO): Promise<number>;
}
