/**
 * Modelo de usuário
 */
import mongoose from 'mongoose';
import {Usuario} from '../../../domain/model/Usuario';

const opts: mongoose.SchemaOptions = {
  collection: 'usuarios',
  toJSON: {
    virtuals: true,
  },
  timestamps: {},
};

const schema = new mongoose.Schema({
  nome: {
    type: String,
    required: true,
  },

  cpf: {
    type: String,
    required: true,
  },

  email: {
    type: String,
  },

  telefone: {
    type: String,
  },

  papeis: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Papel',
  }],
}, opts);

export const UsuarioModel = mongoose.model<Usuario & mongoose.Document>('Usuario', schema);
