import {ServiceBroker} from 'moleculer';
import mongoose from 'mongoose';

const url = 'mongodb+srv://teste:teste@coopeavi-apps-developme.zqrvn.mongodb.net/myFirstDatabase?retryWrites=true&w=majority';

(async () => {
  await mongoose.connect(url, {useUnifiedTopology: true});

  const broker = new ServiceBroker({
    transporter: 'TCP',
  });
  broker.loadServices('modules/**', '*.service.ts');
  broker.repl();
  broker.start();
})();
