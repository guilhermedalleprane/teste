import { IOrdenacao } from "./IOrdenacao"
import { IPaginacao } from "./IPaginacao"
import { IPesquisa } from "./IPesquisa"

export interface IReadOptions {
	paging?: IPaginacao;
	search?: IPesquisa;
	sort?: IOrdenacao;
}